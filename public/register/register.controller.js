var app = angular.module("leads.register.controller", []);
app.controller('RegisterController', ['$scope','Auth','$location','Ref', function($scope,Auth,$location,Ref){
    $scope.register = function() {
    	$scope.message = null;
     	$scope.error = null;
     	Auth.$createUserWithEmailAndPassword($scope.email,$scope.password).then(function(userData) {
     		var bussines = btoa($scope.bussines);
     		var bussines_modules = {};
     		var user = userData.uid;
     		bussines_modules[bussines] = {type_user:{seo:true},modules:{leads:true,pizzeria:true,admin:true}};
			var databaseUsers = Ref.database("users/"+userData.uid);
			var databaseBussines = Ref.database("bussines/"+bussines);
        	$scope.message = "User created with uid: " + userData.uid;
        	// validar que la empresa no exista
			databaseUsers.set({
				firstName:$scope.firstName,
				lastName:$scope.lastName,
				email: $scope.email,
				plans:{pay:false,last_pay:false,type_pay:'month',plan:'free'},
				bussines:bussines_modules
			});
			databaseBussines.set({
				name:$scope.bussines,
				modules:{leads:true,pizzeria:true,admin:true},
				users:[user]
			});
        	$location.path("/");
		}).catch(function(error) {
			$scope.error = error;
		});
    };
}]);