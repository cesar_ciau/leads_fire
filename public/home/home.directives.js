var app = angular.module('leads.home.directives', []);

app.directive('topMenu', function() {
  return {
    restrict: 'E',
    controller: function($scope, $element, $attrs,$timeout, $mdSidenav, $log) {
    	$scope.toggleLeft = buildDelayedToggler('left');
	    $scope.close = function () {
	      // Component lookup should always be available since we are not using `ng-if`
	      $mdSidenav('left').close()
	        .then(function () {
	          $log.debug("close LEFT is done");
	        });

	    };
	    function debounce(func, wait, context) {
	      var timer;

	      return function debounced() {
	        var context = $scope,
	            args = Array.prototype.slice.call(arguments);
	        $timeout.cancel(timer);
	        timer = $timeout(function() {
	          timer = undefined;
	          func.apply(context, args);
	        }, wait || 10);
	      };
	    }
	    function buildDelayedToggler(navID) {
	      return debounce(function() {
	        // Component lookup should always be available since we are not using `ng-if`
	        $mdSidenav(navID)
	          .toggle()
	          .then(function () {
	            $log.debug("toggle " + navID + " is done");
	          });
	      }, 200);
	    }
    },
    templateUrl: '/menus/views/top-menu.html'
  };
});

app.directive('leftMenu', function() {
  return {
    restrict: 'E',
    templateUrl: '/menus/views/left-menu.html'
  };
});