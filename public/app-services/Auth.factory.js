var app = angular.module("authFactory", []);
app.factory("Auth", ["$firebaseAuth",
  function($firebaseAuth) {
    var ref = firebase.database().ref();
    var auth = $firebaseAuth();
    return auth;
  }
]);
app.factory("Ref", ["$firebaseAuth",
  function($firebaseAuth) {
  	return{
  		database: function(rute){
  			var ref = firebase.database().ref(rute);
  			return ref;
  		}
  	}
  }
]);