﻿var app = angular.module("leads.login.controller", []);
// app.factory("Auth", ["$firebaseAuth",
//   function($firebaseAuth) {
//     var ref = firebase.database().ref();
//     var auth = $firebaseAuth();
//     return auth;
//   }
// ]);

app.controller('LoginController', ['$scope','Auth','$location','Ref','$firebaseObject',
  function($scope,Auth,$location,Ref,$firebaseObject){
    $scope.login = function() {
        $scope.authData = null;
        $scope.error = null;
        Auth.$signInWithEmailAndPassword($scope.email, $scope.password).then(function(authData) {
          $scope.authData = authData;
          $location.path("/")
        }).catch(function(error) {
          $scope.error = error;
        });
      };
}]);